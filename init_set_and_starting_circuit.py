import qiskit

def initial_settings():
    ''' set initial settings: number of external qubits, mode'''
    print('Welcome to the Bloch Duel!')
    print("We'll ask you for putting some initial settings.")
    print("First choose the number of auxiliary cubits:")
    aux = int(input()) #number of auxiliary qubits
    print('Now choose game mode:')
    print('If you want to play to a certain score, press "S".')
    print('If you want to play a certain number of moves, press "M".')

    while True:
        mode = input("Mode: ") #variable indicating the mode
        if mode == "S":
            max_score = int(input("Select maximum score to win the game: "))
            mode_tuple = (mode, max_score) #tuple including mode and maximal available score
            break
        elif mode == "M":
            max_move = int(input("Select maximum number of moves: "))
            mode_tuple = (mode, max_move) #tuple including mode and maximal available number of movements
            break
        else:
            print("Wrong key, please try again.")

    final_settings = [aux, mode_tuple] #list including number of auxiliary qubits and the previously declared tuple

    return final_settings


def start_circuit(init_set):
    '''create circuit and qubits and set them to |0>, create players'''
    aux = init_set[0]
    q_player_1 = qiskit.QuantumRegister(1, 'Player 1')
    q_player_2 = qiskit.QuantumRegister(1, 'Player 2')
    auxiliars = qiskit.QuantumRegister(aux, 'q_aux')
    creg = qiskit.ClassicalRegister(2+aux, 'creg')

    circ = qiskit.QuantumCircuit(q_player_1, q_player_2, auxiliars, creg)

    return circ


init = initial_settings()
print(init)
start_circuit(init)